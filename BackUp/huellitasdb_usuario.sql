-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: huellitasdb
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `Nick` varchar(20) NOT NULL,
  `Correo` varchar(30) NOT NULL,
  `Clave` varchar(20) NOT NULL,
  PRIMARY KEY (`Nick`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('aalban','aalban@gmail.com','aejbfuwiiahw'),('aanie','aanie@outlook.com','hncjvbdsh363'),('acarba','acarba@gmail.com','jjbdbyu3gyu'),('acongo','acongo@gmail.com','njvhbsdj7e'),('aramos','aramos@gmail.com','nfbweruhuy378'),('arolo','arolo@hotmail.com','djbfbsdhfiu3h'),('ateo','ateo@hotmail.com','8hyrfgbyud'),('ayuza','ayuza@hotmail.com','jhdfgbhjew83'),('bguor','bguor@gmail.com','bdsfbuiiwehiu'),('bnulo','bnulo@outlook.com','3yr298yhfu4'),('bsantana','bsantana@gmail.com','3iu4iyhbfhe'),('candrade','candrade@gmail.com','dbjsdbf38'),('cdoque','cdoque@gmail.com','jndfg78y'),('cmanpa','cmanpa@yahoo.com','389yhfuwegdfy'),('cmerpa','cmerpa@gmail.com','jbdgbyu'),('cmocas','cmocas@gmail.com','hniufh893y892'),('cracuz','cracruz@outlook.com','jbduysfgyus'),('crito','crito@yahoo.com','uydgfo23trgfbe'),('dguerrero','dguerrero@gmail.com','bhjdbfhweyu'),('dlopez','dlopez@yahoo.com','jbsdjfbiu3h'),('dmocoe','dmocoe@gmail.com','hdfgyu47yf'),('droye','droye@gmail.com','ihf4387tfg43'),('dsoor','dsoor@gmail.com','bg326t62fvv'),('ecastro','ecastro@hotmail.com','njsbfeeru37y34'),('ehergon','ehergon@outlook.com','hdjnfhbwygy3'),('etenesaca','etenesaca@outlook.com','rb3ygby3v'),('fkego','fkego@gmail.com','kbdbfdsgy'),('gcayetano','gcayetano@hotmail.com','djfuq3dqubd'),('gheor','gheor@hotmail.com','ejbyfdgyu'),('jcalcor','jcalcor@hotmail.com','iwuefh4ehu'),('jjorsar','jjorsar@hotmail.com','jjchvbdhsbusd897'),('jpemen','jpemen@gmail.com','28gcgdbyg'),('jsanchez','jsanchez@yahoo.com','jbhusdgfyuwb'),('jsanram','jsanram@outlook.com','qjbhjbfhjbwey'),('jvillacres','jvillacres@gmail.com','efbewhjbf'),('kmascote','kmascote@gmail.com','sjagfyq3g'),('knusan','knusan@gmail.com','jhsdfbdkj02'),('kquintero','kquintero@gmail.com','njdbfwgyeg'),('lricas','lricas@gmail.com','nf7847842'),('mcandia','mcandia@gmail.com','rjbdbfugye'),('mlili','mlili@outlook.com','yehgdewfu'),('mvalo','mvalo@gmail.com','ygdct3tg76d'),('mzambrano','mzambrano@gmail.com','dfibqwbef'),('omencha','omencha@gmail.com','dbjhdbsfbuye'),('pcasji','pcasji@yahoo.com','nsdjfbweu'),('pplame','pplame@yahoo.com','jdhfyu4'),('rjaru','rjaru@hotmail.com','d7824trg46fg2'),('rperez','rperez@outlook.com','bsdbfuierhui3'),('rtete','rtete@outlook.com','jcs73ciu7t'),('sgoju','sgoju@yahoo.com','ndjfbsuyg'),('sponce','sponce@hotmail.com','ndiufh983y'),('squive','squive@outlook.com','jkbjfvsd67'),('tvinu','tvinu@hotmail.com','hdfuihq98y'),('vherrera','vherrera@gmail.com','bhbfrgbuwebu3'),('wflores','wflores@gmail.com','kjbsdfgu3yed'),('xchimi','xchimi@outlook.com','jd9f8y4h98h3'),('xgacha','sgacha@hotmail.com','buydgf8723784t'),('xvebe','xvebe@gmail.com','jbdwbri3g4y'),('zcece','zcece@gmail.com','hf748yt8734bf'),('zvasquez','zvasquez@gmail.com','niuahf873y');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-03  3:12:04
