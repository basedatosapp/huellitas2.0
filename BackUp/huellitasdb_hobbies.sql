-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: huellitasdb
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hobbies`
--

DROP TABLE IF EXISTS `hobbies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hobbies` (
  `ID_Mascota` char(10) NOT NULL,
  `Hobbies` varchar(100) NOT NULL,
  PRIMARY KEY (`ID_Mascota`),
  CONSTRAINT `hobbies_ibfk_1` FOREIGN KEY (`ID_Mascota`) REFERENCES `mascota` (`ID_Mascota`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hobbies`
--

LOCK TABLES `hobbies` WRITE;
/*!40000 ALTER TABLE `hobbies` DISABLE KEYS */;
INSERT INTO `hobbies` VALUES ('0000000001','hobbie1,hobbie3,hobbie4,hobbie5'),('0000000002','hobbie5,hobbie2,hobbie8'),('0000000003','hobbie10,hobbie9'),('0000000004','hobbie3,hobbie7,hobbie9,hobbie2'),('0000000005','hobbie1,hobbie6'),('0000000006','hobbie8,hobbie4,hobbie9,hobbie2'),('0000000007','hobbie7,hobbie3'),('0000000008','hobbie1,hobbie6,hobbie7'),('0000000009','hobbie4,hobbie2,hobbie10'),('0000000010','hobbie3'),('0000000011','hobbie2,hobbie8,hobbie3,hobbie9,hobbie10'),('0000000012','hobbie2,hobbie9,hobbie3'),('0000000013','hobbie4,hobbie8,hobbie10'),('0000000014','hobbie6,hobbie2'),('0000000015','hobbie1,hobbie2'),('0000000016','hobbie8'),('0000000017','hobbie4,hobbie8,hobbie5'),('0000000018','hobbie10,hobbie7,hobbie8,hobbie3'),('0000000019','hobbie1,hobbie3,hobbie9'),('0000000020','hobbie9'),('0000000021','hobbie10,hobbie5,hobbie8'),('0000000022','hobbie2,hobbie10'),('0000000023','hobbie3,hobbie8,hobbie2'),('0000000024','hobbie8'),('0000000025','hobbie3,hobbie9,hobbie1'),('0000000026','hobbie6,hobbie2');
/*!40000 ALTER TABLE `hobbies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-03  3:11:58
