-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: huellitasdb
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctor` (
  `ID_D` char(10) NOT NULL,
  `Nick` varchar(20) NOT NULL,
  `Nombre_Doctor` varchar(50) NOT NULL,
  `Horas_T` int NOT NULL,
  PRIMARY KEY (`ID_D`),
  KEY `Nick` (`Nick`),
  CONSTRAINT `doctor_ibfk_1` FOREIGN KEY (`Nick`) REFERENCES `usuario` (`Nick`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES ('0900274623','squive','Sara Martina Quito Velasco',10),('0916259379','cdoque','Carmen Marcela Domínguez Quevedo',72),('091722917','xvebe','Xavier Sergio Vera Berrones',246),('0917563256','ecastro','Elvira Rosario Castro López',328),('0921389889','dguerrero','Damián Raúl Guerrero Urgilés',147),('0926586521','bsantana','Bryan Jesús Santana Contreras',150),('0927451730','acongo','Adriana Karla Contreras Gómez',420),('0928165663','jvillacres','Julio Ezequiel Villacrés Flores',25),('0936546252','dsoor','Delfina Margarita Solís Orozco',86),('0937465246','sponce','Sandra Pamela Ponce Andrade',158),('0937641723','mcandia','Maribelle Juliana Candia Torres',59),('0938462220','knusan','Karen Pamela Núñez Sánchez',389),('0945567324','ayuza','Ariel Franco Yute Zamora',68),('0947263535','bnulo','Bella Stephanie Núñez López',258),('0952143541','candrade','Casandra Elizabeth Andrade Andrade',276),('0954214561','bguor','Beatriz Emilia Gutierrez Ortíz',30),('0976524673','mvalo','Mayra Alejandra Vanegas Lozano',301),('0981267523','wflores','Wellington Ismael Flores Suco',269),('0983628346','ateo','Andrea Dennise Tenesaca Ochoa',287),('0999372174','cmanpa','Carlos Julio Manzano Palacios',593);
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-03  3:11:57
