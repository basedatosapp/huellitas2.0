-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: huellitasdb
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `datos`
--

DROP TABLE IF EXISTS `datos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `datos` (
  `Cédula` char(10) NOT NULL,
  `NombreCompleto` varchar(50) NOT NULL,
  `F_Nac` date NOT NULL,
  `Dirección` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Cédula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datos`
--

LOCK TABLES `datos` WRITE;
/*!40000 ALTER TABLE `datos` DISABLE KEYS */;
INSERT INTO `datos` VALUES ('0911108002','Daniel Raúl Morales Coello','2000-07-06','Samanes 5 Mz. 948 sl. 11'),('0911271188','Jandry Manuel Jordan Sarmiento','1964-02-19','19 y la F'),('0913767308','Oscar Maximiliano Mendoza Chávez','2001-12-05','Garzota Mz. 18 sl.3'),('0915442985','Ashley Giorgina Carchi Badillo','1988-07-24','Alborada 11 etapa Mz. 5 sl. 9'),('0915675365','Roberto Kenny Pérez Rosado','1989-01-14','Argentina 1800 y José Mascote'),('0916258826','Maritza Javiera Lima Lima','1995-03-29','Urb. Girasoles Mz. 24 sl. 3'),('0916735435','Jonathan Saúl Pezo Mendoza','1999-06-08','Alborada 5 Mz. 1 sl. 11'),('0916756733','Martha Cristina Zambrano Reyes','1984-03-11','Sauces 7 Mz. D5 v. 9'),('0917228356','Saúl Israel Gómez Juca','1998-07-12','Mucho Lote 6 Mz. 948 sl. 11'),('0917253015','Gabriela Tatiana Herrera Ortega','1993-07-26','Samanes 5 Mz. 2 sl. 19'),('0917267920','Camila Stephanie Merchán Palacios','1975-11-15','28 y La P'),('0917314350','Claudia Concepción Morales Castro','1986-12-07','Samanes 7 Mz. 48 sl. 8'),('0917835819','Pedro Rafael Castillo Jiménez','1990-09-30','Esmeralda entre San Martín y Letamendi'),('0918020131','Tamara Milea Vivar Nuñez','1975-11-28','La 28 entre Oriente y el Oro'),('0918246347','Xiomara Alexandra Chiquito Mite','1992-10-10','Alborada 3 etapa Mz. 948 sl. 11'),('0918673241','Evelyn Vanessa Hernández González','1978-05-19','26 y la A'),('0918673245','Emilio James Hernández Gonzáles','1996-03-05','Samanes 4 Mz. 402 sl. 6'),('0923756622','Zoila Verónica Cevallos Cevallos','1988-12-18','Pablo Vélez y Quevedo'),('0924741363','Daniela Bertha Román Yépez','1960-08-04','14 y Domingo Comín'),('0926352816','Gabriela Geanella Cayetano Aguirre','1979-04-14','Coop. Colinas de la Alborada Mz. 725 v. 9'),('0926936636','Carlos Alberto Ramírez Cuzco','1968-09-18','Pedro Carbo 1800 y Machala'),('0927518641','José Pablo Sánchez Vélez','1999-04-06','Cdla. Huancavilca Sur Mz. 3D v. 11'),('0927563456','Ana Lucía Rodríguez López','1990-04-08','44 y la P'),('0927632378','Ayleen Xiomara Ramos Hidalgo','1988-02-28','Samanes 5 Mz. 10 sl. 11'),('0927729143','Frank Camilo Kenecevitch Gómez','1999-01-26','Samanes 13 Mz. 7 sl. 1'),('0928473629','Aarón Alberto Albán Orozco','1983-05-05','Recreo 1 coop. 28 de Agosto'),('0932715648','Linda Fernanda Rivero Castro','1999-03-09','Samanes 1 Mz. 8 sl. 1'),('0936466290','Ricardo Walter Telo Telo','1997-05-15','Capitán Najera #124 y Machala'),('0937426562','Roxana Graciela Jácome Ruíz','1965-11-30','27 y la CH'),('0937462839','Katherine Juliette Mascote López','1984-11-06','39 y Nicolás Augusto Gonzáles'),('0937467233','José Manuel Sánchez Ramírez','1991-09-23','17 y Portete'),('0937562521','Daniel Eduardo López Burgos','1969-06-12','Samanes 4 Mz. 8 sl. 1'),('0940938848','Jhossue David Calderón Córdova','1988-11-05','27 y Galápagos'),('0946275652','Viviana Cecilia Herrera José','1999-08-10','Letamendi y Los Ríos'),('0947182950','Alanyz Nicole Álava Nieto','1987-04-02','Mucho Lote 5 Mz. 948 sl. 11'),('0947286163','Pilar Daniela Placencio Mera','1994-07-20','44 y la P'),('0954214561','Beatriz Emilia Gutierrez Ortíz','1966-01-24','Samanes 1 Mz. 28 sl. 2'),('0963256546','Cristhian Paúl Rivadeneira Torres','1997-04-28','Urb. EcoCity Mz. 20 sl. 10'),('0967735260','Xavier Alesandro Galindo Chávez','1990-07-28','Ceibos Norte Mz. 24 sl. 13'),('0973264752','Keyla Fabiana Quintero Ruíz','2000-08-18','24 y Sedalana'),('0973973628','Zuly Patricia Vásquez Jurado','1976-10-27','Urbanización Guasmo Sur Coop. Guayas y Quil 2 Mz. 877 sl. 8'),('0998364872','Eduardo Luis Tenesaca Vera','1989-10-31','Samanes 6 Mz. 948 sl. 11');
/*!40000 ALTER TABLE `datos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-03  3:12:03
