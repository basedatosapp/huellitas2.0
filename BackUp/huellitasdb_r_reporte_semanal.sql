-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: huellitasdb
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `r_reporte_semanal`
--

DROP TABLE IF EXISTS `r_reporte_semanal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `r_reporte_semanal` (
  `ID_Reporte` char(10) NOT NULL,
  `Cédula` char(10) NOT NULL,
  `ID_Mascota` char(10) NOT NULL,
  `FechaCreación` date NOT NULL,
  `Descripción` text NOT NULL,
  `Foto_Mascota` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID_Reporte`),
  KEY `Cédula` (`Cédula`),
  KEY `ID_Mascota` (`ID_Mascota`),
  CONSTRAINT `r_reporte_semanal_ibfk_1` FOREIGN KEY (`Cédula`) REFERENCES `adoptante` (`Cédula`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `r_reporte_semanal_ibfk_2` FOREIGN KEY (`ID_Mascota`) REFERENCES `mascota` (`ID_Mascota`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `r_reporte_semanal`
--

LOCK TABLES `r_reporte_semanal` WRITE;
/*!40000 ALTER TABLE `r_reporte_semanal` DISABLE KEYS */;
INSERT INTO `r_reporte_semanal` VALUES ('0000000001','0928473629','0000000001','2020-06-02','Descripción1','foto1.png'),('0000000002','0916756733','0000000002','2020-07-08','Descripción2','foto2.png'),('0000000003','0918673245','0000000003','2020-08-11','Descripción3','foto3.png'),('0000000004','0917835819','0000000004','2020-07-03','Descripción4','foto4.png'),('0000000005','0937467233','0000000005','2020-08-04','Descripción5','foto5.png'),('0000000006','0928473629','0000000017','2020-06-11','Descripción6','foto6.png'),('0000000007','0998364872','0000000016','2020-07-22','Descripción7','foto7.png'),('0000000008','0937467233','0000000005','2020-08-16','Descripción8','foto8.png'),('0000000009','0918673245','0000000012','2020-06-18','Descripción9','foto9.png'),('0000000010','0946275652','0000000008','2020-07-26','Descripción10','foto10.png'),('0000000011','0928473629','0000000017','2020-07-31','Descripción11','foto11.png'),('0000000012','0967735260','0000000011','2020-07-10','Descripción12','foto12.png'),('0000000013','0916756733','0000000009','2020-07-31','Descripción13','foto13.png'),('0000000014','0911108002','0000000013','2020-06-27','Descripción14','foto14.png'),('0000000015','0917253015','0000000014','2020-08-02','Descripción15','foto15.png'),('0000000016','0947182950','0000000015','2020-07-26','Descripción16','foto16.png'),('0000000017','0911108002','0000000018','2020-07-11','Descripción17','foto17.png'),('0000000018','0928473629','0000000001','2020-07-02','Descripción18','foto18.png'),('0000000019','0916756733','0000000026','2020-06-15','Descripción19','foto19.png'),('0000000020','0917253015','0000000020','2020-08-11','Descripción20','foto20.png'),('0000000021','0937426562','0000000021','2020-08-01','Descripción21','foto21.png'),('0000000022','0916756733','0000000026','2020-06-25','Descripción22','foto22.png'),('0000000023','0923756622','0000000022','2020-07-08','Descripción23','foto23.png'),('0000000024','0998364872','0000000025','2020-08-11','Descripción24','foto24.png'),('0000000025','0963256546','0000000023','2020-06-06','Descripción25','foto25.png'),('0000000026','0924741363','0000000024','2020-07-28','Descripción26','foto26.png'),('0000000027','0928473629','0000000001','2020-08-22','Descripción27','foto27.png');
/*!40000 ALTER TABLE `r_reporte_semanal` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-03  3:12:02
