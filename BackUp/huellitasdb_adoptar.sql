-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: huellitasdb
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adoptar`
--

DROP TABLE IF EXISTS `adoptar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `adoptar` (
  `N_Solicitud` char(5) NOT NULL,
  `Cédula` char(10) NOT NULL,
  `Inf_Hogar` text NOT NULL,
  `Patio` tinyint(1) NOT NULL,
  `CartaRecomendación1` text,
  `CartaRecomentación2` text,
  PRIMARY KEY (`N_Solicitud`),
  KEY `Cédula` (`Cédula`),
  CONSTRAINT `adoptar_ibfk_1` FOREIGN KEY (`Cédula`) REFERENCES `adoptante` (`Cédula`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adoptar`
--

LOCK TABLES `adoptar` WRITE;
/*!40000 ALTER TABLE `adoptar` DISABLE KEYS */;
INSERT INTO `adoptar` VALUES ('00001','0928473629','Inf_Hogar_1',1,'CartaRecomendación1',NULL),('00002','0928473629','Inf_Hogar_2',1,'CartaRecomendación1','CartaRecomendación2'),('00003','0916756733','Inf_Hogar_3',1,'CartaRecomendación1','CartaRecomendación2'),('00004','0998364872','Inf_Hogar_4',1,'CartaRecomendación1','CartaRecomendación2'),('00005','0918673245','Inf_Hogar_5',0,'CartaRecomendación1',NULL),('00006','0917835819','Inf_Hogar_6',1,'CartaRecomendación1',NULL),('00007','0937467233','Inf_Hogar_7',0,'CartaRecomendación1','CartaRecomendación2'),('00008','0967735260','Inf_Hogar_8',0,'CartaRecomendación1',NULL),('00009','0926352816','Inf_Hogar_9',0,'CartaRecomendación1',NULL),('00010','0998364872','Inf_Hogar_10',1,'CartaRecomendación1','CartaRecomendación2'),('00011','0946275652','Inf_Hogar_11',1,'CartaRecomendación1','CartaRecomendación2'),('00012','0918020131','Inf_Hogar_12',1,'CartaRecomendación1','CartaRecomendación2'),('00013','0967735260','Inf_Hogar_13',0,'CartaRecomendación1','CartaRecomendación2'),('00014','0926936636','Inf_Hogar_14',0,'CartaRecomendación1',NULL),('00015','0947182950','Inf_Hogar_15',0,'CartaRecomendación1',NULL),('00016','0963256546','Inf_Hogar_16',0,NULL,'CartaRecomendación2'),('00017','0917253015','Inf_Hogar_17',1,'CartaRecomendación1','CartaRecomendación2'),('00018','0947182950','Inf_Hogar_18',0,'CartaRecomendación1','CartaRecomendación2'),('00019','0911108002','Inf_Hogar_19',1,'CartaRecomendación1','CartaRecomendación2'),('00020','0918246347','Inf_Hogar_20',0,'CartaRecomendación1',NULL),('00021','0963256546','Inf_Hogar_21',0,'CartaRecomendación1','CartaRecomendación2'),('00022','0937426562','Inf_Hogar_22',0,NULL,'CartaRecomendación2'),('00023','0937426562','Inf_Hogar_23',0,'CartaRecomendación1','CartaRecomendación2'),('00024','0923756622','Inf_Hogar_24',1,'CartaRecomendación1','CartaRecomendación2'),('00025','0911108002','Inf_Hogar_25',1,'CartaRecomendación1','CartaRecomendación2'),('00026','0924741363','Inf_Hogar_26',1,'CartaRecomendación1','CartaRecomendación2'),('00027','0918246347','Inf_Hogar_27',0,'CartaRecomendación1','CartaRecomendación2'),('00028','0932715648','Inf_Hogar_28',0,NULL,'CartaRecomendación2'),('00029','0928473629','Inf_Hogar_29',1,'CartaRecomendación1','CartaRecomendación2'),('00030','0916756733','Inf_Hogar_30',1,'CartaRecomendación1','CartaRecomendación2'),('00031','0918673245','Inf_Hogar_31',0,NULL,'CartaRecomendación2'),('00032','0918673245','Inf_Hogar_32',0,'CartaRecomendación1','CartaRecomendación2'),('00033','0917835819','Inf_Hogar_33',1,'CartaRecomendación1','CartaRecomendación2'),('00034','0937467233','Inf_Hogar_34',0,'CartaRecomendación1','CartaRecomendación2'),('00035','0917253015','Inf_Hogar_17',1,'CartaRecomendación1','CartaRecomendación2'),('00036','0916756733','Inf_Hogar_30',1,'CartaRecomendación1','CartaRecomendación2'),('00037','0946275652','Inf_Hogar_11',1,'CartaRecomendación1','CartaRecomendación2');
/*!40000 ALTER TABLE `adoptar` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-03  3:12:04
