-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: huellitasdb
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mensaje`
--

DROP TABLE IF EXISTS `mensaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mensaje` (
  `ID_Mensaje` char(10) NOT NULL,
  `Cédula_Emp` char(10) NOT NULL,
  `Cédula_Adopt` char(10) NOT NULL,
  `Content` text NOT NULL,
  `F_SMS` date NOT NULL,
  PRIMARY KEY (`ID_Mensaje`),
  KEY `Cédula_Emp` (`Cédula_Emp`),
  KEY `Cédula_Adopt` (`Cédula_Adopt`),
  CONSTRAINT `mensaje_ibfk_1` FOREIGN KEY (`Cédula_Emp`) REFERENCES `empleado` (`Cédula`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mensaje_ibfk_2` FOREIGN KEY (`Cédula_Adopt`) REFERENCES `adoptante` (`Cédula`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mensaje`
--

LOCK TABLES `mensaje` WRITE;
/*!40000 ALTER TABLE `mensaje` DISABLE KEYS */;
INSERT INTO `mensaje` VALUES ('0000000001','0927632378','0963256546','No ha enviado ningún reporte','2020-08-10'),('0000000002','0973973628','0998364872','Sólo ha enviado un reporte','2020-08-01'),('0000000003','0911271188','0911108002','Sólo ha enviado un reporte','2020-08-06'),('0000000004','0927632378','0967735260','Sólo ha enviado un reporte','2020-08-19'),('0000000005','0940938848','0917835819','Sólo ha enviado un reporte','2020-08-03'),('0000000006','0911271188','0918020131','No ha enviado ningún reporte','2020-08-08'),('0000000007','0915442985','0917253015','Sólo ha enviado un reporte','2020-08-28'),('0000000008','0973264752','0947182950','Sólo ha enviado un reporte','2020-08-10'),('0000000009','0973973628','0911108002','Sólo ha enviado un reporte','2020-08-04'),('0000000010','0916735435','0918246347','No ha enviado ningún reporte','2020-08-01'),('0000000011','0973264752','0963256546','Sólo ha enviado un reporte','2020-08-28'),('0000000012','0927632378','0937426562','Sólo ha enviado un reporte','2020-08-28'),('0000000013','0916258826','0923756622','Sólo ha enviado un reporte','2020-08-02'),('0000000014','0915442985','0924741363','Sólo ha enviado un reporte','2020-08-01'),('0000000015','0911271188','0918673245','Sólo ha enviado un reporte','2020-08-05'),('0000000016','0911271188','0918020131','No ha enviado ningún reporte','2020-08-28'),('0000000017','0927632378','0963256546','No ha enviado ningún reporte','2020-08-26'),('0000000018','0973973628','0998364872','Sólo ha enviado un reporte','2020-08-18'),('0000000019','0940938848','0917253015','Sólo ha enviado un reporte','2020-08-20'),('0000000020','0973264752','0947182950','Sólo ha enviado un reporte','2020-08-27'),('0000000021','0973973628','0918673245','Sólo ha enviado un reporte','2020-08-30');
/*!40000 ALTER TABLE `mensaje` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-03  3:12:06
