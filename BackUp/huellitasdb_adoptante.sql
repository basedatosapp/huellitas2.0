-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: huellitasdb
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adoptante`
--

DROP TABLE IF EXISTS `adoptante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `adoptante` (
  `Cédula` char(10) NOT NULL,
  `Nick` varchar(20) NOT NULL,
  `Foto` varchar(20) NOT NULL,
  PRIMARY KEY (`Cédula`),
  KEY `Nick` (`Nick`),
  CONSTRAINT `adoptante_ibfk_1` FOREIGN KEY (`Cédula`) REFERENCES `datos` (`Cédula`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `adoptante_ibfk_2` FOREIGN KEY (`Nick`) REFERENCES `usuario` (`Nick`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adoptante`
--

LOCK TABLES `adoptante` WRITE;
/*!40000 ALTER TABLE `adoptante` DISABLE KEYS */;
INSERT INTO `adoptante` VALUES ('0911108002','dmocoe','imagen14.png'),('0916756733','mzambrano','imagen2.png'),('0917253015','gheor','imagen12.png'),('0917835819','pcasji','imagen4.png'),('0918020131','tvinu','imagen9.png'),('0918246347','xchimi','imagen15.png'),('0918673245','ehergon','imagen3.png'),('0923756622','zcece','imagen18.png'),('0924741363','droye','imagen19.png'),('0926352816','gcayetano','imagen6.png'),('0926936636','cracuz','imagen11.png'),('0928473629','aalban','imagen1.jpg'),('0932715648','lricas','imagen20.png'),('0937426562','rjaru','imagen17.png'),('0937467233','jsanram','imagen5.png'),('0946275652','vherrera','imagen8.png'),('0947182950','aanie','imagen13.png'),('0963256546','crito','imagen16.png'),('0967735260','xgacha','imagen10.png'),('0998364872','etenesaca','imagen7.png');
/*!40000 ALTER TABLE `adoptante` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-03  3:12:03
