/*
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controls;


import ec.edu.espol.common.Adoptante;
import ec.edu.espol.common.Doctor;
import ec.edu.espol.common.Empleado;
import ec.edu.espol.common.Usuario;
import ec.edu.espol.db.CreateConnection;
import ec.edu.espol.huellitas_app.App;
import ec.edu.espol.utility.Teléfonos;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author danny
 */
public class EmpleadoController implements Initializable {

    private String clase;
    private Usuario usuario;
    private Empleado e;
    @FXML
    private TextField nombreC, Dir, Tel, Ced;
    @FXML
    private DatePicker date;
    @FXML
    private Button create;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    public void setBasics(Usuario u) {
        usuario = u;
        clase = "Empleado";
        
    }
    private void cargarInfo() {
        String nombre = nombreC.getText();
        String cedula = Ced.getText();
        String dir = Dir.getText();
        String[] datos = {cedula, nombre, dir};
        Date d = java.sql.Date.valueOf(date.getValue());
        String tel = Tel.getText(); 
        e = new Empleado(datos, d, tel, usuario.getDatos());
    }
    
    @FXML
    public void Create(MouseEvent event) {
        cargarInfo();
        
        try {
            CreateConnection x = new CreateConnection();
            Connection c = x.getCon();
            Statement st = c.createStatement();
            c.setAutoCommit(false);
            String query_usuario = "insert into usuario values " + usuario.toString();
            boolean create_usuario = st.execute(query_usuario);
            if (!create_usuario) c.rollback();
            String query_datos = "insert into datos values " + e.getStrinDatos();
            boolean create_datos = st.execute(query_datos);
            if (!create_datos) c.rollback();
            String query_empleado = "insert into empleado values " + e.toString();
            boolean create_empleado = st.execute(query_empleado);
            if (!create_empleado) c.rollback();
            if (create_usuario && create_datos && create_empleado) {
                Alert a = new Alert(Alert.AlertType.CONFIRMATION, "REGISTRO EXITOSO");
                a.show();
                try {
                    FXMLLoader fxml = App.loadFXMLLoad("Login");
                    App.setRoot(fxml);
                    LoginController lc = fxml.getController();
                    x.getCon().close();
                    lc.setBasics(usuario);
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            } else {
                c.rollback();
                Alert a = new Alert(Alert.AlertType.CONFIRMATION, "ERROR AL REGISTRAR");
                a.show();
            }        
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo establecer conexión a la base de datos...");
            a.show();
        }
       
    }

     
}
