/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controls;

import ec.edu.espol.common.Adoptante;
import ec.edu.espol.common.Usuario;
import ec.edu.espol.db.CreateConnection;
import ec.edu.espol.huellitas_app.App;
import java.awt.Component;
import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * FXML Controller class
 *
 * @author danny
 */
public class ActualizarDatosController implements Initializable {

    @FXML
    private Label ruta;
    @FXML
    private TextField ced, nick, email;
    @FXML
    private DatePicker date;
    @FXML
    private TextField nombreC;
    @FXML
    private TextField Tel;
    @FXML
    private TextField Dir;
    @FXML
    private Button Subir;
    @FXML
    private Button create, reg;
    @FXML
    private PasswordField password;
    
    private Usuario usuario;
    private String nick_previo;
    private Adoptante adoptante;
    private String cedula_previa;
    private CreateConnection con;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }
    @FXML
    public void Regresar() {       
        try {
            FXMLLoader fxml = App.loadFXMLLoad("Principal");
            App.setRoot(fxml);
            PrincipalController pc = fxml.getController();
            pc.setBasics(usuario, adoptante, con);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
    @FXML
    public void CargarIMG(){
        try {            
            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & GIF", "jpg", "gif");
            fileChooser.setFileFilter(filter);
            int seleccion = fileChooser.showOpenDialog((Component)Subir.getUserData());
            if (seleccion == JFileChooser.APPROVE_OPTION) {                
                File fichero = fileChooser.getSelectedFile();
                ImageIcon imgIcon = new ImageIcon(fichero.getPath());
                ruta.setText(fichero.getPath());
                //Acciones que se quieran realizar
            }
            Alert a = new Alert(AlertType.CONFIRMATION, "Imagen cargada con éxito");
            a.show();
        } catch (HeadlessException | SecurityException e) {
            Alert a = new Alert(AlertType.ERROR, "Error al cargar la imagen...");
            a.show();
            System.out.println(e.getMessage());
            System.out.println(e.toString());
        }
        
    }
    
    @FXML
    private void Ejecutar() {
        try {           
            Statement state = con.getCon().createStatement(); System.out.println("Statement");
            String cedula = ced.getText();System.out.println("Ced"); System.out.println(cedula);
            String nombre = nombreC.getText(); System.out.println("NombreC"); System.out.println(nombre);
            String fecha = date.getValue().toString(); System.out.println("Date"); System.out.println(fecha);
            String direc = Dir.getText(); System.out.println("Dir"); System.out.println(direc);
            boolean cedula_bool = cedula_previa.equals(cedula);
            System.out.println("Cedula_BOOL");
            int cont = 0;
            // Datos
            if (cedula_bool) {
                System.out.println(cedula_bool);
                String query =
                "update datos set NombreCompleto =\"" + nombre + "\" " +
                ", F_Nac = \'" + fecha + "\' , Dirección = \"" + direc + "\" " +
                "where Cédula = \"" + cedula_previa + "\";";
                int d = state.executeUpdate(query);
                System.out.println();
                System.out.println(d); cont = cont + d; System.out.println(cont);
            } else {
                System.out.println(cedula_bool);
                String query = 
                "update datos set Cédula = \"" + cedula + "\" ,  NombreCompleto =\"" + nombre + "\" " +
                ", F_Nac = \'" + fecha + "\' , Dirección = \"" + direc + "\" " +
                "where Cédula = \"" + cedula_previa + "\";";
                int d = state.executeUpdate(query);
                System.out.println(query);
                System.out.println(d); cont = cont + d; System.out.println(cont);
            }
            String us = String.valueOf(nick.getText()); System.out.println("us" + us);
            String co = String.valueOf(email.getText()); System.out.println("co"+ co);
            String cl = String.valueOf(password.getText());; System.out.println("cl"+cl);
            boolean usuario_bool = nick_previo.equals(us);
            System.out.println("Usuarioo BOOL");
            // Usuario
            if (usuario_bool) {
                System.out.println(usuario_bool);
                String query =
                "update usuario set Correo =\"" + co + "\" , Clave = \"" + cl + "\" " +
                "where Nick = \"" + nick_previo + "\";";
                int u = state.executeUpdate(query);
                System.out.println(query);
                System.out.println(u); cont = cont + u; System.out.println(cont);
            } else {
                System.out.println(usuario_bool);
                String query =
                "update usuario set Nick = \"" + us + "\" , Correo =\"" +  co + "\" , Clave = \"" + cl + "\" " +
                "where Nick = \"" + nick_previo + "\";";
                int u = state.executeUpdate(query);
                System.out.println(query);
                System.out.println(u); cont = cont + u; System.out.println(cont);
            }
            String PATH = ruta.getText();
            // Adoptante
            if (cedula_bool && usuario_bool) {
                System.out.println("cedula_bool && usuario_bool");             
                System.out.println("Ojo");
                String query =
                "update adoptante set Foto = \"" + PATH + "\" " +
                "where Cédula = \"" + cedula_previa + "\" and Nick = \"" + nick_previo + "\";";
                int a = state.executeUpdate(query);
                System.out.println(query);
                System.out.println(a); cont = cont + a; System.out.println(cont);
            } else if (!cedula_bool && usuario_bool){
                System.out.println("Ojo --U");
                String query =
                "SET sql_mode = \'\'; " +
                "update adoptante set Foto = \"" + PATH + "\" " +
                "where Cédula = \"" + cedula + "\" and Nick = \"" + nick_previo + "\";";
                int a = state.executeUpdate(query);
                System.out.println(query);
                System.out.println(a); cont = cont + a; System.out.println(cont);
            } else if (!cedula_bool && !usuario_bool){
                System.out.println("Ojo --U");
                String query = 
                "SET sql_mode = \'\'; " +
                "update adoptante set Foto = \"" + PATH + "\" " +
                "where Cédula = \"" + cedula + "\" and Nick = \"" + us+ "\";";
                int a = state.executeUpdate(query);
                System.out.println(query);
                System.out.println(a); cont = cont + a; System.out.println(cont);
            } else if (cedula_bool && !usuario_bool){
                System.out.println("Ojo --U");
                String query =
                "SET sql_mode = \'\'; " +
                "update adoptante set Foto = \"" + PATH + "\" " +
                "where Cédula = \"" + cedula_previa + "\" and Nick = \"" + us+ "\";";
                int a = state.executeUpdate(query);
                System.out.println(query);
                System.out.println(a); cont = cont + a; System.out.println(cont);
            }
            if (cont == 3) {
                Alert al = new Alert(AlertType.CONFIRMATION, "Actualización exitosa");
                al.show();
                Regresar();
            } else {
                Alert al = new Alert(AlertType.ERROR, "Error al actualizar");
                al.show();
                
            }
            //state.close();
        } catch (SQLException e) {
            System.out.println("Exception...........................");
            System.out.println(e.getMessage());
            System.out.println(e.toString());
            System.out.println(e.getErrorCode());
        }
        
    }    
    public void setBasics(Usuario user, Adoptante ad, CreateConnection connection){
        System.out.println("SetBasics");
        usuario = user;
        nick_previo = usuario.getUsuario();
        nick.setText(usuario.getUsuario()); 
        email.setText(usuario.getCorreo());
        password.setText(usuario.getClave());
        adoptante = ad;
        con = connection;     
        cedula_previa = adoptante.getCed();
        ced.setText(adoptante.getCed()); 
        nombreC.setText(adoptante.getNombreCompleto()); 
        Dir.setText(adoptante.getDirección());
        ruta.setText(adoptante.getPath());
        
        System.out.println(con.getLD());
        date.setValue(con.getLD());
        System.out.println("DateDATE");
        
        try {
            Tel.setText(adoptante.getT());
        } catch(Exception e) {
            Tel.setText("");
            System.out.println(e.toString());
        }
        
    }
    /*
    private void setTeléfonos() {
        String tel = Tel.getText(); 
        contador = tel.contains(",");
        if (contador) {
            String[] tel_arr = tel.split(",");
            ArrayList<String> tel_lista = new ArrayList<>();
            for(String k: tel_arr) {
                tel_lista.add(k);
            }
            Teléfonos teléfonos = new Teléfonos(tel_lista);
            adoptante.setTel(teléfonos);
        }
        Teléfonos t = new Teléfonos(tel);
        adoptante.setTel(t);

    }
    private void setInfo() {
        String[] datos_usuario = {usuario.getUsuario(), usuario.getCorreo(), usuario.getClave()};
        String[] datos = {this.ced.getText(), this.nombreC.getText(), this.Dir.getText()};
        adoptante = new Adoptante(datos, date.getValue(), datos_usuario);
        System.out.println(adoptante.getStrinDatos());
        //System.out.println(adoptante.getFechaNacimiento());
        
    }
    private void guardarInfo() {
        setInfo();
        setTeléfonos();
    }
    */
}
