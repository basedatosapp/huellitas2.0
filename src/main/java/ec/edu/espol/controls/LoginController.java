/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controls;

import ec.edu.espol.common.Usuario;
import ec.edu.espol.db.CreateConnection;
import ec.edu.espol.huellitas_app.App;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javax.swing.UIManager;

/**
 * FXML Controller class
 *
 * @author danny
 */
public class LoginController implements Initializable {

    @FXML
    private Button login;
    @FXML
    private TextField Nick;
    @FXML
    private TextField Email;
    @FXML
    private PasswordField Password;
    @FXML
    private Label sign_up;
    
    private Usuario user;
    private CreateConnection con;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        UIManager.setInstalledLookAndFeels(UIManager.getInstalledLookAndFeels());
        try {
            con = new CreateConnection();
            
        } catch (SQLException e) {
            System.out.println(e.toString());
            System.out.println(e.getMessage());
        }
        // TODO
    }
    @FXML
    public void change_to_SignUp() {
        try {
            FXMLLoader fxml = App.loadFXMLLoad("SignUp");
            App.setRoot(fxml);
            SignUpController suc = fxml.getController();
            suc.setBasics(Nick.getText(), Email.getText(), Password.getText(), con);
        } catch(IOException e) {
            System.out.println(e.getMessage());
            System.out.println(e.toString());
            System.out.println(e.getCause().toString());
        }        
    }
    @FXML
    public void Login(MouseEvent event) {
        boolean b = con.validateLogin(Nick.getText(), Email.getText(), Password.getText());
        if (b) {
            System.out.println("Validation Successful");
            String[] datos_u = {Nick.getText(), Email.getText(), Password.getText()};
            user = new Usuario(datos_u);
            try {
                System.out.println("5666");
                FXMLLoader fxml = App.loadFXMLLoad("Principal"); System.out.println("Principalaaaalll");
                App.setRoot(fxml); System.out.println("fxml");
                PrincipalController pc = fxml.getController(); System.out.println("controler");
                pc.setBasics(user, con.getAdoptante(user), con); System.out.println("setbasics");
            } catch (IOException e) {
                System.out.println(e.getMessage());
                Alert a = new Alert(AlertType.ERROR, "No se pudo acceder a la ventana Main...");
                a.show();
            }
        } else {
            Alert a = new Alert(AlertType.ERROR, "Ingresó mal las credenciales...");
            a.show();
        }              
    }
    public void setBasics(String u, String e, String c, CreateConnection connection) {
        Nick.setText(u);
        Email.setText(e);
        Password.setText(c);
    }
    public void setBasics(Usuario u) {
        user = u;
        Nick.setText(user.getUsuario());
        Email.setText(user.getCorreo());
        Password.setText(user.getClave());
    }
}
