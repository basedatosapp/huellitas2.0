/*
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controls;

import ec.edu.espol.common.Adoptante;
import ec.edu.espol.common.Doctor;
import ec.edu.espol.common.Usuario;
import ec.edu.espol.db.CreateConnection;
import ec.edu.espol.huellitas_app.App;
import ec.edu.espol.utility.Teléfonos;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author danny
 */
public class DoctorController implements Initializable {

    private String clase;
    private Usuario usuario;
    private Doctor doctor;
    @FXML
    private TextField id, nombre;
    @FXML
    private ComboBox horasT;
    @FXML
    private Button create;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ArrayList<String> lista = new ArrayList<>();
        for(int i=1;i<10;i++){
            if (i==1) lista.add(i+" Hora");
            lista.add(i+" Horas");
        }
        horasT.setItems((ObservableList) lista);
        
        // TODO
    }
    public void setBasics(Usuario u) {
        usuario = u;
        clase = "Doctor";
        id.setText(usuario.getUsuario().substring(0,5)+"D");
    }
    private void cargarInfo() {
        String nombreD = nombre.getText();
        int HorasT = (int)horasT.getValue();
        doctor = new Doctor(id.getText(), nombreD, HorasT, usuario.getDatos());
    }
    @FXML
    public void Create(ActionEvent event) {
        cargarInfo();
        
        try {
            CreateConnection x = new CreateConnection();
            Connection c = x.getCon();
            Statement st = c.createStatement();
            c.setAutoCommit(false);
            String query_usuario = "insert into usuario values " + usuario.toString();
            boolean create_usuario = st.execute(query_usuario);
            if (!create_usuario) c.rollback();
            String query_adoptante = "insert into doctor values " + doctor.toString();
            boolean create_doctor = st.execute(query_adoptante);
            if (!create_doctor) c.rollback();
            if (create_usuario && create_doctor) {
                Alert a = new Alert(AlertType.CONFIRMATION, "REGISTRO EXITOSO");
                a.show();
                try {
                    FXMLLoader fxml = App.loadFXMLLoad("Login");
                    App.setRoot(fxml);
                    LoginController lc = fxml.getController();
                    x.getCon().close();
                    lc.setBasics(usuario);
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            } else {
                c.rollback();
                Alert a = new Alert(AlertType.CONFIRMATION, "ERROR AL REGISTRAR");
                a.show();
            }        
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Alert a = new Alert(AlertType.ERROR, "No se pudo establecer conexión a la base de datos...");
            a.show();
        }
       
    }

     
}