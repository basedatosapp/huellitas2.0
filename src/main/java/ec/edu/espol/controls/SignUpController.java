/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controls;

import ec.edu.espol.common.Usuario;
import ec.edu.espol.db.CreateConnection;
import ec.edu.espol.huellitas_app.App;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author danny
 */
public class SignUpController implements Initializable {

    @FXML
    private Button createAccount;
    @FXML
    private TextField Nick;
    @FXML
    private TextField Email;
    @FXML
    private PasswordField Password;
    @FXML
    private Label login;
    @FXML
    private ComboBox type;
    
    private Usuario user;
    private String clase = "NoClass";
    private CreateConnection con;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Adoptante");
        lista.add("Doctor");
        lista.add("Empleado");
        type.setItems(FXCollections.observableArrayList(lista));
        // TODO
    }
    private boolean validate() {
        return (Nick.getText().equals("") || Email.getText().equals("") || Password.getText().equals("") || clase.equals("NoCalss"));
    }
    @FXML
    public void CreateAccount() {
        
        try {            
            if (validate()) {
                FXMLLoader fxml = App.loadFXMLLoad("Login");
                App.setRoot(fxml);
            } else {
                String[] datos_u = {Nick.getText(), Email.getText(), Password.getText()};
                user = new Usuario(datos_u);
                switch (clase) {
                    case "NoClass":
                        try {
                            FXMLLoader fxml = App.loadFXMLLoad("Login");
                            App.setRoot(fxml);
                        } catch(IOException e) {
                            System.out.println(e.getMessage());
                            System.out.println(e.getCause().toString());
                            System.out.println(e.toString());
                        }
                        break;
                    case "Adoptante":
                        try {
                            FXMLLoader fxml = App.loadFXMLLoad("Adoptante");
                            App.setRoot(fxml);
                            AdoptanteController ac = fxml.getController();
                            ac.setBasics(user, con);
                        } catch(IOException e) {
                            System.out.println(":(");
                            System.out.println(e.getMessage());
                            System.out.println(e.getCause().toString());
                            System.out.println(e.toString());
                        }
                        break;
                    case "Doctor":
                        try {
                            FXMLLoader fxml = App.loadFXMLLoad("Doctor");
                            App.setRoot(fxml);
                            DoctorController dc = fxml.getController();
                            dc.setBasics(user);
                        } catch(IOException e) {
                            System.out.println(e.getMessage());
                            System.out.println(e.getCause().toString());
                            System.out.println(e.toString());
                        }
                        break;
                    case "Empleado":
                        try {
                            FXMLLoader fxml = App.loadFXMLLoad("Empleado");
                            App.setRoot(fxml);
                            EmpleadoController ec = fxml.getController();
                            ec.setBasics(user);
                        } catch(IOException e) {
                            System.out.println(e.getMessage());
                            System.out.println(e.getCause().toString());
                            System.out.println(e.toString());
                        }
                        break;
                }
            }
        } catch(IOException e) {
            System.out.println("CREATE METHOD   *");
            System.out.println(e.getMessage());
            System.out.println(e.getCause().toString());
            System.out.println(e.toString());
        }
        
    }
    
    @FXML
    public void setClase(ActionEvent event) {
        ComboBox cb = (ComboBox) event.getSource();
        clase = (String)cb.getValue();
    }
    
    public void setBasics(String u, String e, String c, CreateConnection connection) {
        Nick.setText(u);
        Email.setText(e);
        Password.setText(c);
        con = connection;
    }
    public void setBasics(Usuario u, CreateConnection connection) {
        user = u;
        con = connection;
    }
    
    @FXML
    public void change_to_Login() {
        try {
            FXMLLoader fxml = App.loadFXMLLoad("Login");
            App.setRoot(fxml);
            LoginController lc = fxml.getController();
            lc.setBasics(Nick.getText(), Email.getText(), Password.getText(), con);
        } catch(IOException e) {
            System.out.println(e.getMessage());
            System.out.println(e.toString());
            System.out.println(e.getCause().toString());
        }
    }    
}
