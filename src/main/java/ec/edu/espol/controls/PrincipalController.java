package ec.edu.espol.controls;

import ec.edu.espol.common.Adoptante;
import ec.edu.espol.common.Usuario;
import ec.edu.espol.db.CreateConnection;
import ec.edu.espol.huellitas_app.App;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
public class PrincipalController {
    
    private CreateConnection x;
    private Connection con;
    private Usuario User;
    private Adoptante A;
    private ResultSet rs;
    @FXML
    private Button logout, logout1;
    @FXML
    private TextField nick, eli, eliCed;
    @FXML
    private TextField email;
    @FXML
    private TextArea textArea;
    @FXML
    private ComboBox cbx;
    
    
    
    public void setBasics(Usuario u, Adoptante ad, CreateConnection c) {
        User = u;
        nick.setText(User.getUsuario());
        email.setText(User.getCorreo());
        A = ad;
        x = c;
        con = x.getCon();
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Adoptantes");
        lista.add("Adoptantes sin reportes");
        cbx.setItems(FXCollections.observableArrayList(lista));
        show_adoptantes();
    }
    @FXML
    public void ActualizarDatos(MouseEvent event) {
        try {
            FXMLLoader fxml = App.loadFXMLLoad("ActualizarDatos");
            App.setRoot(fxml);
            ActualizarDatosController acc = fxml.getController();
            acc.setBasics(User, A, x);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
    @FXML
    public void LogOut(MouseEvent event) {
        try {
            con.close();
            FXMLLoader fxml = App.loadFXMLLoad("Login");
            App.setRoot(fxml);
        } catch (IOException | SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    @FXML
    private void cargarViews() {
        switch((String)cbx.getValue()) {
            case("Adoptantes"):
                show_adoptantes();
                break;
            case("Adoptantes sin reportes"):
                show_view();
                break;
        }
    }
    @FXML
    private void actualizar() {
        this.setBasics(User, A, x);
    }
    private void show_view() {
        System.out.println("show view");
        
        String r = ""; System.out.println("R");
        try {
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery("select * from adop_sin_reporte;");
            System.out.println("While");
            while (rs.next()) {
                r = r + rs.getString("Cédula") + "-"; System.out.println("Ced");
                r = r + rs.getString("NombreCompleto")+ "-"; System.out.println("NomC");
                r = r + rs.getString("F_Nac")+ "-"; System.out.println("F_Nac");
                r = r + rs.getString("Dirección")+ "-"; System.out.println("Dir");
                r = r + rs.getString("id_mascota") + "\n"; System.out.println("idM");
                System.out.println("rs.next");
                System.out.println(rs.getMetaData());
                System.out.println(rs.toString());
            }
            textArea.setText(r);
            //state.close();
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        
    }
    @FXML
    private void Eliminar() {
        try {
            Statement state = con.createStatement(); System.out.println("State");
            int eje = state.executeUpdate("delete from usuario where Nick = \"" + eli.getText() + "\";"); System.out.println("eje");
            System.out.println(eje);
            int ejex2 = state.executeUpdate("delete from datos where Cédula = \"" + eliCed.getText() + "\";"); System.out.println("ejex2");
            System.out.println(ejex2);
            int respuesta = eje + ejex2;
            if (respuesta == 2) {
                Alert a = new Alert(AlertType.CONFIRMATION, "Eliminado con éxito");
                a.show();
            } else {
                Alert a = new Alert(AlertType.ERROR, "Error al eliminar");
                a.show();
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
            System.out.println("Exection");
        }
    }

    private void show_adoptantes() {
        System.out.println("show view");        
        String r = ""; System.out.println("R");
        try {
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery(
            "select * from (adoptante a natural join datos d natural join usuario u) where a.Cédula = d.Cédula and u.Nick = a.Nick;");
            System.out.println("While");
            while (rs.next()) {
                r = r + rs.getString("Nick") + "-";
                r = r + rs.getString("Cédula")+ "-";
                r = r + rs.getString("Foto")+ "-";
                r = r + rs.getString("NombreCompleto")+ "-";
                r = r + rs.getString("F_Nac") + "-";
                r = r + rs.getString("Dirección")+ "-";
                r = r + rs.getString("Correo")+ "\n";
                System.out.println("rs.next");
                System.out.println(rs.getMetaData());
                System.out.println(rs.toString());
                
                
            }
            textArea.setText(r);
            //state.close();
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        
    }
}