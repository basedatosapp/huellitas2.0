/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controls;

import ec.edu.espol.common.Adoptante;
import ec.edu.espol.common.Usuario;
import ec.edu.espol.db.CreateConnection;
import ec.edu.espol.huellitas_app.App;
import ec.edu.espol.utility.Teléfonos;
import java.awt.Component;
import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * FXML Controller class
 *
 * @author danny
 */
public class AdoptanteController implements Initializable {

    @FXML
    private Label ruta;
    @FXML
    private TextField ced;
    @FXML
    private DatePicker date;
    @FXML
    private TextField nombreC;
    @FXML
    private TextField Tel;
    @FXML
    private TextField Dir;
    @FXML
    private Button Subir;
    @FXML
    private Button create;
    
    private Usuario usuario;
    private Adoptante adoptante;
    private boolean contador;
    private CreateConnection con;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    public void CargarIMG(MouseEvent event){
        try {            
            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & GIF", "jpg", "gif");
            fileChooser.setFileFilter(filter);
            int seleccion = fileChooser.showOpenDialog((Component)Subir.getUserData());
            if (seleccion == JFileChooser.APPROVE_OPTION) {                
                File fichero = fileChooser.getSelectedFile();
                ImageIcon imgIcon = new ImageIcon(fichero.getPath());
                ruta.setText(fichero.getPath());
                //Acciones que se quieran realizar
            }
            Alert a = new Alert(AlertType.CONFIRMATION, "Imagen cargada con éxito");
            a.show();
        } catch (HeadlessException | SecurityException e) {
            Alert a = new Alert(AlertType.ERROR, "Error al cargar la imagen...");
            a.show();
            System.out.println(e.getMessage());
            System.out.println(e.toString());
        }
        
    }
    @FXML
    private void Ejecutar() {
        boolean b = false;
        try {
            Connection x = con.getCon(); System.out.println("x.getCon");
            Statement state = x.createStatement(); System.out.println("Statement");
            System.out.println("ResultSet");
            ResultSet rs = state.executeQuery("select * from usuario where Nick = \"" + usuario.getUsuario() + "\" and Correo = \"" + usuario.getCorreo() +"\";");
            //ResultSet rs = state.executeQuery("select * from usuario where Nick = \"dsoria\" and Correo = \"dsoria@espol.edu.ec\";");
            System.out.println("?????????????????????????????????????????????????????????????");
            String n, e;
            try {
                n = rs.getString("Nick"); System.out.println(n);
                e = rs.getString("Correo"); System.out.println(e);
            } catch (Exception ex) {
                n = ":v"; System.out.println(n);
                e = "xd"; System.out.println(e);
                System.out.println(ex.toString());
            }
            
            System.out.println("?????????????????????????????????????????????????????????????");
            if (n.equals(usuario.getUsuario())) {
                Alert a = new Alert(AlertType.ERROR, "Usuario ya existente"); System.out.println("Nick existente");
                a.show();
                b = false;
            } else if (e.equals(usuario.getCorreo())) {
                Alert a = new Alert(AlertType.ERROR, "Correo ya existente"); System.out.println("Correo existente");
                a.show();
                b = false;
            } else {
                System.out.println("Ninguna");
                b = true;
            }
            rs.close();
            state.close();
        } catch (SQLException e) {
            System.out.println("Exception...........................");
            System.out.println(e.getMessage());
            System.out.println(e.toString());
        }
        if (b) Create();
    } 
    private void Create() {
        try {
            con.getCon().setAutoCommit(false);
            Statement state = con.getCon().createStatement();
            guardarInfo(); System.out.println("guardarInfo");
            adoptante.setPath(ruta.getText()); System.out.println("Set path");
            int u = state.executeUpdate("insert into usuario values " + usuario.toString());
            System.out.println("Insersión de Usuario: " + u);
            System.out.println(usuario.toString());
            System.out.println("Ingresando D");            
            int d = state.executeUpdate(adoptante.getStrinDatos());
            System.out.println(d);
            System.out.println("Insersión de Usuario: ");
            System.out.println("Insertando A");
            int ad = state.executeUpdate("insert into adoptante values " + adoptante.toString());
            System.out.println(ad);
            System.out.println("Inserción de Adoptante:");
            /*
            String query_tel = "insert into teléfonos values "; System.out.println("Ingresando Tel");            
            boolean create_tel = false; System.out.println("boolean Tel: " + create_tel + "¡?");
            System.out.println(contador);
            try {
                if (contador) {
                    query_tel = query_tel + adoptante.getStringListaTel(con.getCon());
                    System.out.println("Ingresando Tel -");
                    create_tel = Domain.insert(con, query_tel);
                    System.out.println("Insertando Tel: " + create_tel);
                } else {
                    query_tel = query_tel + adoptante.getStringTel(con.getCon());
                    System.out.println("Ingresando Tel --");
                    create_tel = Domain.insert(con, query_tel);
                    System.out.println("Insertando Tel: " + create_tel);
                }
            } catch(Exception exc) {
                System.out.println(exc.getMessage());
                System.out.println(exc.toString());
            }
            */
            if (u == 1 && d == 1 && ad == 1) {
                Alert a = new Alert(AlertType.CONFIRMATION, "REGISTRO EXITOSO"); System.out.println("Ingreso exitoso");
                a.show();
                con.getCon().commit();
                try {
                    System.out.println("Try login");
                    FXMLLoader fxml = App.loadFXMLLoad("Login"); System.out.println("Load login");
                    App.setRoot(fxml);
                    LoginController lc = fxml.getController();
                    lc.setBasics(usuario);
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                    System.out.println(ex.toString());
                }
            } else {
                con.getCon().rollback();
                Alert a = new Alert(AlertType.ERROR, "ERROR AL REGISTRAR");
                a.show();
            }
            
            state.close();
        } catch (SQLException e) {            
            System.out.println(e.toString());
            System.out.println(e.getMessage());
        }
    }
    
    public void setBasics(Usuario user, CreateConnection connection){
        usuario = user;
        con = connection;
        System.out.println(usuario.toString());
    }
    private void setTeléfonos() {
        String tel = Tel.getText(); 
        contador = tel.contains(",");
        if (contador) {
            String[] tel_arr = tel.split(",");
            ArrayList<String> tel_lista = new ArrayList<>();
            for(String k: tel_arr) {
                tel_lista.add(k);
            }
            Teléfonos teléfonos = new Teléfonos(tel_lista);
            adoptante.setTel(teléfonos);
        }
        Teléfonos t = new Teléfonos(tel);
        adoptante.setTel(t);

    }
    private void setInfo() {
        String[] datos_usuario = {usuario.getUsuario(), usuario.getCorreo(), usuario.getClave()};
        String[] datos = {this.ced.getText(), this.nombreC.getText(), this.Dir.getText()};
        adoptante = new Adoptante(datos, date.getValue(), datos_usuario);
        System.out.println(adoptante.getStrinDatos());
        //System.out.println(adoptante.getFechaNacimiento());
        
    }
    private void guardarInfo() {
        setInfo();
        setTeléfonos();
    }
    
}
