/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.common;


import ec.edu.espol.utility.Adoptar;
import ec.edu.espol.utility.Cita;
import ec.edu.espol.utility.Reporte;
import ec.edu.espol.utility.Teléfonos;
import java.awt.image.BufferedImage;
import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author danny
 */
public class Adoptante extends Usuario {
    private String Ced, NombreCompleto, Dirección;
    private Date FechaNacimiento;
    private String date;
    private File Foto;
    private String Path;
    private BufferedImage foto;
    private ArrayList<Adoptar> lista;
    private ArrayList<Reporte> lista_reporte;
    private ArrayList<Cita> lista_citas;
    private Teléfonos tel;
    public Adoptante(String[] datos, Date F, BufferedImage foto, Teléfonos tel, String[] datos_usuario) {
        super(datos_usuario);
        this.Ced = datos[0];
        this.NombreCompleto = datos[1];
        this.Dirección = datos[2];
        this.FechaNacimiento = F;
        this.tel = tel;
        this.foto = foto;
    }
    public Adoptante(String[] datos, Date F, File foto, Teléfonos tel, String[] datos_usuario) {
        super(datos_usuario);
        this.Ced = datos[0];
        this.NombreCompleto = datos[1];
        this.Dirección = datos[2];
        this.FechaNacimiento = F;
        this.tel = tel;
        this.Foto = foto;
    }
    public Adoptante(String[] datos, Date F, String foto, Teléfonos tel, String[] datos_usuario) {
        super(datos_usuario);
        this.Ced = datos[0];
        this.NombreCompleto = datos[1];
        this.Dirección = datos[2];
        this.FechaNacimiento = F;
        this.tel = tel;
        this.Path = foto;
    }
    public Adoptante(String[] datos, Date F, String foto, String[] datos_usuario) {
        super(datos_usuario);
        this.Ced = datos[0];
        this.NombreCompleto = datos[1];
        this.Dirección = datos[2];
        this.FechaNacimiento = F;
        this.Path = foto;
    }
    public Adoptante(String[] datos, Date F, String[] datos_usuario) {
        super(datos_usuario);
        this.Ced = datos[0];
        this.NombreCompleto = datos[1];
        this.Dirección = datos[2];
        this.FechaNacimiento = F;
    }
    public Adoptante(String[] datos, String F, String[] datos_usuario) {
        super(datos_usuario);
        this.Ced = datos[0];
        this.NombreCompleto = datos[1];
        this.Dirección = datos[2];
        this.date = F;
    }
    public Adoptante(String[] datos, Object F, String[] datos_usuario) {
        super(datos_usuario);
        this.Ced = datos[0];
        this.NombreCompleto = datos[1];
        this.Dirección = datos[2];
        LocalDate ld = (LocalDate)F;
        this.date = ld.toString();
    }
    public Teléfonos getTel() {
        return tel;
    }

    public void setTel(Teléfonos tel) {
        this.tel = tel;
    }
    
    public BufferedImage getBI() {
        return foto;
    }
    public void setBI(BufferedImage bi) {
        this.foto = bi;
    }
    public ArrayList<Cita> getLista_citas() {
        return lista_citas;
    }

    public void setLista_citas(ArrayList<Cita> lista_citas) {
        this.lista_citas = lista_citas;
    }
    public ArrayList<Reporte> getLista_reporte() {
        return lista_reporte;
    }

    public void setLista_reporte(ArrayList<Reporte> lista_reporte) {
        this.lista_reporte = lista_reporte;
    }

    public ArrayList<Adoptar> getLista() {
        return lista;
    }

    public void setLista(ArrayList<Adoptar> lista) {
        this.lista = lista;
    }

    public String getCed() {
        return Ced;
    }

    public void setCed(String Ced) {
        this.Ced = Ced;
    }

    public String getNombreCompleto() {
        return NombreCompleto;
    }

    public void setNombreCompleto(String NombreCompleto) {
        this.NombreCompleto = NombreCompleto;
    }

    public String getDirección() {
        return Dirección;
    }

    public void setDirección(String Dirección) {
        this.Dirección = Dirección;
    }
    
    public Date getFechaNacimiento() {
        return FechaNacimiento;
    }
    public String getF() {
        return date.toString();
    }
    public void getF(String f) {
       this.date = f;
    }
    public void setFechaNacimiento(Date FechaNacimiento) {
        this.FechaNacimiento = FechaNacimiento;
    }
    public void setPath(String p) {
        this.Path = p;
    }
    public String getPath() {
        return this.Path;
    }
    public File getFoto() {
        return Foto;
    }

    public void setFoto(File Foto) {
        this.Foto = Foto;
    }
    
    public String getT() {
        return this.tel.getTels();
    }
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Adoptante other = (Adoptante) obj;
        if (!Objects.equals(this.Ced, other.Ced)) {
            return false;
        }
        return true;
    }
    public String getStrinDatos() {
        return "insert into datos values (" + "\"" + this.getCed() + "\"" + ","
                   + "\"" + this.getNombreCompleto() + "\"" + ","
                   + "\'" + this.getF() + "\'" + "," 
                   + "\"" + this.getDirección() + "\"" + ");";
    }
    /*
    public String getStringListaTel(Connection x) {
        String telef = "";
        for (String t: tel.getLista()) {
            telef = telef + "(" + Domain.nextCodeTel(x) + "\"" + t + "\"" + "," + "\"" + this.getCed() + "\"" + ")";
            if (lista.get(lista.size()-1).equals(t)) {
                telef = telef + ";";
            } else {
                telef = telef + ",";
            }
        }
        return telef;
    }
    /*
    public String getStringTel(Connection x) {
        return "(" + Domain.nextCodeTel(x) + "\"" + tel.getTels() + "\"" + "," + "\"" + this.getCed() + "\"" + ");";
    }
    */
    @Override
    public String toString() {
        return "(" + "\"" + this.getCed() + "\"" + ","
                   + "\"" + this.getUsuario() + "\"" + ","
                   + "\"" + this.getPath() + "\"" + ");";
    }
    
}
