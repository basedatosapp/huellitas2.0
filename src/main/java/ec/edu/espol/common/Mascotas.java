/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.common;

import ec.edu.espol.utility.Adoptar;
import ec.edu.espol.utility.Cita;
import ec.edu.espol.utility.Hobbies;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Objects;

/**
 *
 * @author danny
 */
public class Mascotas {
    private String id_m, raza;
    protected Date f_nacimiento;
    protected Hobbies hobbies;
    private Adoptar adopt;
    private ArrayList<Cita> lista;
    public Mascotas(String id_m, String raza, Date F, Adoptar adopt, Hobbies hobbies){
        this.id_m = id_m;
        this.raza = raza;
        this.adopt = adopt;
        this.f_nacimiento = F;
        this.hobbies = hobbies;
    }
    public Mascotas(String id_m, String raza, Date F, Hobbies hobbies){
        this.id_m = id_m;
        this.raza = raza;
        this.f_nacimiento = F;
        this.hobbies = hobbies;
    }
    public Mascotas(String id_m, String raza, Date F){
        this.id_m = id_m;
        this.raza = raza;
        this.f_nacimiento = F;
    }
    public ArrayList<Cita> getLista() {
        return lista;
    }

    public void setLista(ArrayList<Cita> lista) {
        this.lista = lista;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mascotas other = (Mascotas) obj;
        if (!Objects.equals(this.id_m, other.id_m)) {
            return false;
        }
        return true;
    }

    public String getId_m() {
        return id_m;
    }

    public void setId_m(String id_m) {
        this.id_m = id_m;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public Adoptar getAdopt() {
        return adopt;
    }

    public void setAdopt(Adoptar adopt) {
        this.adopt = adopt;
    }

    public Date getF_nacimiento() {
        return f_nacimiento;
    }

    public void setF_nacimiento(Date f_nacimiento) {
        this.f_nacimiento = f_nacimiento;
    }

    public Hobbies getHobbies() {
        return hobbies;
    }

    public void setHobbies(Hobbies hobbies) {
        this.hobbies = hobbies;
    }
    
    
}
