/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.common;

import ec.edu.espol.utility.Cita;
import ec.edu.espol.utility.Mensaje;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author danny
 */
public class Empleado extends Usuario{
    private String cédula;
    private String nombre;
    private Date fecha;
    private String dir;
    private String tels;
    private ArrayList<Cita> lista;
    private ArrayList<Mensaje> lista_m;
    public Empleado(String[] datos, Date F, String tels, String[] datos_usuario) {
        super(datos_usuario);
        this.cédula = datos[0];
        this.nombre = datos[1];
        this.dir = datos[2];
        this.fecha = F;
        this.tels = tels;
    }
    public String getStrinDatos() {
        return "(" + "\"" + this.getCédula() + "\"" + ","
                   + "\"" + this.getNombre() + "\"" + ","
                   + "\"" + this.getFecha().toString() + "\"" + "," 
                   + "\"" + this.getDir() + "\"" + ")";
    }
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empleado other = (Empleado) obj;
        if (!Objects.equals(this.cédula, other.cédula)) {
            return false;
        }
        return true;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getCédula() {
        return cédula;
    }

    public void setCédula(String cédula) {
        this.cédula = cédula;
    }

    public ArrayList<Mensaje> getLista_m() {
        return lista_m;
    }

    public void setLista_m(ArrayList<Mensaje> lista_m) {
        this.lista_m = lista_m;
    }

    public ArrayList<Cita> getLista() {
        return lista;
    }

    public void setLista(ArrayList<Cita> lista) {
        this.lista = lista;
    }

    public String getTels() {
        return tels;
    }

    public void setTels(String tels) {
        this.tels = tels;
    }

    @Override
    public String toString() {
        return "(" + "\"" + this.getCédula() + "\"" + ","
                   + "\"" + this.getUsuario()+ "\"" + ","
                   + "\"" + this.getTels() + "\"" + ");";
    }
    
    
}
