/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.common;

import java.util.Objects;

/**
 *
 * @author danny
 */
public class Usuario {
    private String Usuario, Correo, Clave;
    private String[] datos;
    public Usuario(String u, String c, String p) {
        this.Usuario = u;
        this.Correo = c;
        this.Clave = p;
    }
    public Usuario(String[] datos) {
        this.datos = datos;
        this.Usuario = datos[0];
        this.Correo = datos[1];
        this.Clave = datos[2]; 
    }

    public String[] getDatos() {
        return datos;
    }

    public void setDatos(String[] datos) {
        this.datos = datos;
    }
    
    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public String getClave() {
        return Clave;
    }

    public void setClave(String Clave) {
        this.Clave = Clave;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.Usuario, other.Usuario)) {
            return false;
        }
        if (!Objects.equals(this.Correo, other.Correo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "(" + "\"" + this.getUsuario() + "\"" + ","
                   + "\"" + this.getCorreo() + "\"" + ","
                   + "\"" + this.getClave() + "\"" + ")";
    }
    
}
