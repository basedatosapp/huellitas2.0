/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.common;

import ec.edu.espol.utility.Cita;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author danny
 */
public class Doctor extends Usuario{
    private String ID_D;
    private String Nombre;
    private int HorasTrabajadas;
    private ArrayList<Cita> lista;
    public Doctor(String id_d, String n, int horas_t, String[] datos_usuario) {
        super(datos_usuario);
        this.ID_D = id_d;
        this.Nombre = n;
        this.HorasTrabajadas = horas_t;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public ArrayList<Cita> getLista() {
        return lista;
    }

    public void setLista(ArrayList<Cita> lista) {
        this.lista = lista;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Doctor other = (Doctor) obj;
        if (!Objects.equals(this.ID_D, other.ID_D)) {
            return false;
        }
        return true;
    }

    public String getID_D() {
        return ID_D;
    }

    public void setID_D(String ID_D) {
        this.ID_D = ID_D;
    }

    public int getHorasTrabajadas() {
        return HorasTrabajadas;
    }

    public void setHorasTrabajadas(int HorasTrabajadas) {
        this.HorasTrabajadas = HorasTrabajadas;
    }

    @Override
    public String toString() {
        return "(" + "\"" + this.getID_D() + "\"" + ","
                   + "\"" + this.getUsuario()+ "\"" + ","
                   + "\"" + this.getNombre() + "\"" + ","
                   + "\"" + this.getHorasTrabajadas() + ");";
    }
    
}
