/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.utility;

import ec.edu.espol.common.Adoptante;
import ec.edu.espol.common.Mascotas;
import java.awt.image.BufferedImage;
import java.io.File;
import java.sql.Date;
import java.util.Objects;


/**
 *
 * @author danny
 */
public class Reporte {
    private String id_reporte;
    private Mascotas m;
    private Adoptante adopt;
    private Date FechaCreación;
    private File Descripción;
    private File Foto;
    private BufferedImage foto;
    public Reporte(String id_reporte, Mascotas m, Adoptante a, Date F, File d, BufferedImage f) {
        this.id_reporte = id_reporte;
        this.m = m;
        this.adopt = a;
        this.FechaCreación = F;
        this.Descripción = d;
        this.foto = f;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reporte other = (Reporte) obj;
        if (!Objects.equals(this.id_reporte, other.id_reporte)) {
            return false;
        }
        return true;
    }

    public String getId_reporte() {
        return id_reporte;
    }

    public void setId_reporte(String id_reporte) {
        this.id_reporte = id_reporte;
    }

    public Mascotas getM() {
        return m;
    }

    public void setM(Mascotas m) {
        this.m = m;
    }

    public Adoptante getAdopt() {
        return adopt;
    }

    public void setAdopt(Adoptante adopt) {
        this.adopt = adopt;
    }

    public Date getFechaCreación() {
        return FechaCreación;
    }

    public void setFechaCreación(Date FechaCreación) {
        this.FechaCreación = FechaCreación;
    }

    public File getDescripción() {
        return Descripción;
    }

    public void setDescripción(File Descripción) {
        this.Descripción = Descripción;
    }

    public File getFoto() {
        return Foto;
    }

    public void setFoto(File Foto) {
        this.Foto = Foto;
    }
    public BufferedImage getfoto() {
        return foto;
    }

    public void setFoto(BufferedImage Foto) {
        this.foto = Foto;
    }
    
}
