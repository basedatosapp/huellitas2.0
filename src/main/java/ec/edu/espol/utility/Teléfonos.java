/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.utility;

import ec.edu.espol.common.Adoptante;
import java.util.ArrayList;

/**
 *
 * @author danny
 */
public class Teléfonos {
    private int id;
    private String tels;
    private Adoptante adopt;
    private ArrayList<String> lista;
    public Teléfonos(ArrayList<String> lista, Adoptante a){
        this.lista = lista;
        this.adopt = a;
    }
    public Teléfonos(String tels, Adoptante a){
        this.tels = tels;
        this.adopt = a;
    }
    
    public Teléfonos(ArrayList<String> lista){
        this.lista = lista;
    }
    public Teléfonos(String tels){
        this.tels = tels;
    }
    public ArrayList<String> getLista() {
        return lista;
    }

    public void setLista(ArrayList<String> lista) {
        this.lista = lista;
    }
    public boolean isListaNull() {
        return (lista == null);
    }
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Teléfonos other = (Teléfonos) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTels() {
        return tels;
    }

    public void setTels(String tels) {
        this.tels = tels;
    }

    public Adoptante getAdopt() {
        return adopt;
    }

    public void setAdopt(Adoptante adopt) {
        this.adopt = adopt;
    }
    
}
