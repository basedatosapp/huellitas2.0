/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.utility;

import ec.edu.espol.common.Adoptante;
import ec.edu.espol.common.Empleado;
import java.io.File;
import java.sql.Date;
import java.util.Objects;


/**
 *
 * @author danny
 */
public class Mensaje {
    private String id_mensaje;
    private Empleado e;
    private Adoptante a;
    private File Contenido;
    private Date fecha_sms;
    public Mensaje(String id, Empleado e, Adoptante a, File text, Date F) {
        this.id_mensaje =id;
        this.e = e;
        this.a = a;
        this.Contenido = text;
        this.fecha_sms = F;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mensaje other = (Mensaje) obj;
        if (!Objects.equals(this.id_mensaje, other.id_mensaje)) {
            return false;
        }
        return true;
    }

    public String getId_mensaje() {
        return id_mensaje;
    }

    public void setId_mensaje(String id_mensaje) {
        this.id_mensaje = id_mensaje;
    }

    public Empleado getE() {
        return e;
    }

    public void setE(Empleado e) {
        this.e = e;
    }

    public Adoptante getA() {
        return a;
    }

    public void setA(Adoptante a) {
        this.a = a;
    }

    public File getContenido() {
        return Contenido;
    }

    public void setContenido(File Contenido) {
        this.Contenido = Contenido;
    }

    public Date getFecha_sms() {
        return fecha_sms;
    }

    public void setFecha_sms(Date fecha_sms) {
        this.fecha_sms = fecha_sms;
    }
    
}
