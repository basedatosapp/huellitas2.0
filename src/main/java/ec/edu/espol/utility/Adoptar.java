/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.utility;

import ec.edu.espol.common.Adoptante;
import ec.edu.espol.common.Mascotas;
import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author danny
 */
public class Adoptar {
    private String N_Solicitud;
    private Adoptante adopt;
    private File Inf_Hogar;
    private boolean Patio;
    private File CartaR1, CartaR2;
    private ArrayList<Mascotas> lista;
    public Adoptar(Adoptante adopt, ArrayList<Mascotas> m) {
        this.adopt = adopt;
        this.lista = m;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Adoptar other = (Adoptar) obj;
        if (!Objects.equals(this.N_Solicitud, other.N_Solicitud)) {
            return false;
        }
        return true;
    }

    public String getN_Solicitud() {
        return N_Solicitud;
    }

    public void setN_Solicitud(String N_Solicitud) {
        this.N_Solicitud = N_Solicitud;
    }

    public Adoptante getAdopt() {
        return adopt;
    }

    public void setAdopt(Adoptante adopt) {
        this.adopt = adopt;
    }

    public File getInf_Hogar() {
        return Inf_Hogar;
    }

    public void setInf_Hogar(File Inf_Hogar) {
        this.Inf_Hogar = Inf_Hogar;
    }

    public boolean isPatio() {
        return Patio;
    }

    public void setPatio(boolean Patio) {
        this.Patio = Patio;
    }

    public File getCartaR1() {
        return CartaR1;
    }

    public void setCartaR1(File CartaR1) {
        this.CartaR1 = CartaR1;
    }

    public File getCartaR2() {
        return CartaR2;
    }

    public void setCartaR2(File CartaR2) {
        this.CartaR2 = CartaR2;
    }

    public ArrayList<Mascotas> getLista() {
        return lista;
    }

    public void setLista(ArrayList<Mascotas> lista) {
        this.lista = lista;
    }
    
}
