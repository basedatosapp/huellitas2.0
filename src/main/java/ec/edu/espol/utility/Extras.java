package ec.edu.espol.utility;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class Extras {
    //MÉTODO EXTRA UTILITARIO
    public static byte[] getSHA(String i) throws NoSuchAlgorithmException  {
        MessageDigest md = MessageDigest.getInstance("SHA-256"); 
        return md.digest(i.getBytes(StandardCharsets.UTF_8));
    }
    public static String toHexString(byte[] hash) { 
        BigInteger number = new BigInteger(1, hash);
        StringBuilder hexString = new StringBuilder(number.toString(16));
        while (hexString.length() < 32) {  
            hexString.insert(0, '0');  
        }
        return hexString.toString();  
    } 
    public static String clave_Hex(String Clave) {
        try {
            return toHexString(getSHA(Clave));
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
    public static String capitalize(String str) {
        str = str.toLowerCase();
        if(str == null || str.isEmpty()) {
            return str;
        }
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
    public static void serializar(ArrayList<Object> lista, String tipo) throws IOException {
        try(ObjectOutputStream stream_out = new ObjectOutputStream(new FileOutputStream(tipo.toUpperCase()+".ser"));){
            stream_out.writeObject(lista);
            stream_out.flush();
        } catch(IOException e) {
            
        }
        
    }
    
    public static ArrayList<Object> deserializar(String tipo) {
        try(ObjectInputStream stream_in = new ObjectInputStream(new FileInputStream(tipo.toUpperCase()+".ser"));){
            ArrayList<Object> lista = (ArrayList<Object>)stream_in.readObject();
            return lista;
        } catch(Exception e) {
            return null;
        }
    }
}
