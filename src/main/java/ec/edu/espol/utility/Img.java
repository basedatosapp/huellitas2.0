/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.utility;

import java.io.FileReader;
import javafx.scene.control.Alert;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

/**
 *
 * @author danny
 */
public class Img extends javax.swing.JFrame {
    private File archivo;
    private String rutaAbsoluta;
    private BufferedImage img;
    private int h, w;
    public Img() {
        
    }
    public void Upload() {
        JFileChooser jfc = new JFileChooser();
        FileNameExtensionFilter filtro = new FileNameExtensionFilter(".jpg", ".png");
        jfc.setFileFilter(filtro);
        int selection = jfc.showOpenDialog(null);
        if (selection == JFileChooser.APPROVE_OPTION) {
            archivo = jfc.getSelectedFile();
            rutaAbsoluta = archivo.getAbsolutePath();       
            try(FileReader fr = new FileReader(archivo)) {
                String cadena = ""; 
                int valor = fr.read();
                while (valor != -1) {
                    cadena = cadena + (char)valor;
                    valor = fr.read();
                }
                Alert a = new Alert(Alert.AlertType.CONFIRMATION, cadena);
                a.show();
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
    public void cargaImag() {
        try {
            String url = rutaAbsoluta;
            img = ImageIO.read(new File(url));

            w = img.getWidth(); // ancho
            h = img.getHeight(); //alto
            if (img.getType() != BufferedImage.TYPE_INT_RGB) {
                BufferedImage bi2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
                Graphics big = bi2.getGraphics();
                big.drawImage(img, 0, 0, w, h, null);
                System.out.println("Imagen cargada correctamente");
            }
            this.setSize(w, h);
        } catch (IOException e) {
            System.out.println("La imagen no se pudo leer");
            System.out.println(e.getMessage());
        }
    }
    public String getRutaAbs() {
        return rutaAbsoluta;
    }
    public BufferedImage getBI(){
        return img;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Img other = (Img) obj;
        if (!Objects.equals(this.img, other.img)) {
            return false;
        }
        return true;
    }
}
