/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.utility;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.accessibility.AccessibleContext;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JRootPane;
/**
 *
 * @author danny
 */
public class Imagen extends javax.swing.JFrame implements ActionListener{
    private BufferedImage imagen, imagen_filtro, copia;
    int w, h, opcion, grados = 0;
    double x1, y1;

    /**
     * constructor de la clase
     */
    public Imagen() {
    }
    public String agregar_imagen() {

        JFileChooser file = new JFileChooser();//Objeto de tipo File Chosser para seleccionar la ruta de la imagen
        File ruta = null;// como la ruta cambia de direccion, la inicializo a null como contador

        int estado = file.showOpenDialog(null);//guardo el estado en un entero
        if (estado == JFileChooser.APPROVE_OPTION) {//Si presiono en aceptar entonces se procesa a guardar la direccion

            ruta = file.getSelectedFile();
            String exportPath = file.getSelectedFile().getAbsolutePath();
            System.out.println(exportPath);
            return exportPath;
        }
        return null;
    }//fin deñ metodo cargar imagen

    /**
     * metodo que carga la imagen al bufferedImagen ajustando el tamaño de la
     * ventana
     *
     */
    public void cargaImag() {
        try {
            String url = agregar_imagen();
            imagen = ImageIO.read(new File(url));

            w = imagen.getWidth(); // ancho
            h = imagen.getHeight(); //alto
            if (imagen.getType() != BufferedImage.TYPE_INT_RGB) {
                BufferedImage bi2
                        = new BufferedImage(imagen.getWidth(), imagen.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics big = bi2.getGraphics();
                big.drawImage(imagen, 0, 0, w, h, null);
                imagen_filtro = copia = imagen = bi2;
                System.out.println("Imagen cargada correctamente");
            }
            this.setSize(w, h);
        } catch (IOException e) {
            System.out.println("La imagen no se pudo leer");
            //System.exit(1);
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel label;
    // End of variables declaration//GEN-END:variables

    public BufferedImage getImagen() {
        return imagen;
    }

    public void setImagen(BufferedImage imagen) {
        this.imagen = imagen;
    }

    public BufferedImage getImagen_filtro() {
        return imagen_filtro;
    }

    public void setImagen_filtro(BufferedImage imagen_filtro) {
        this.imagen_filtro = imagen_filtro;
    }

    public BufferedImage getCopia() {
        return copia;
    }

    public void setCopia(BufferedImage copia) {
        this.copia = copia;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getOpcion() {
        return opcion;
    }

    public void setOpcion(int opcion) {
        this.opcion = opcion;
    }

    public int getGrados() {
        return grados;
    }

    public void setGrados(int grados) {
        this.grados = grados;
    }

    public double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public double getY1() {
        return y1;
    }

    public void setY1(double y1) {
        this.y1 = y1;
    }

    public JLabel getLabel() {
        return label;
    }

    public void setLabel(JLabel label) {
        this.label = label;
    }

    @Override
    public JRootPane getRootPane() {
        return rootPane;
    }

    @Override
    public void setRootPane(JRootPane rootPane) {
        this.rootPane = rootPane;
    }

    @Override
    public boolean isRootPaneCheckingEnabled() {
        return rootPaneCheckingEnabled;
    }

    @Override
    public void setRootPaneCheckingEnabled(boolean rootPaneCheckingEnabled) {
        this.rootPaneCheckingEnabled = rootPaneCheckingEnabled;
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        return accessibleContext;
    }

    public void setAccessibleContext(AccessibleContext accessibleContext) {
        this.accessibleContext = accessibleContext;
    }
    public String getRutaAbs() {
        return "Ruta";
    }
    
}
