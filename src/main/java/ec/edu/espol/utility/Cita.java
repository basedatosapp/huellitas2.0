/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.utility;

import ec.edu.espol.common.Doctor;
import ec.edu.espol.common.Mascotas;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

/**
 *
 * @author danny
 */
public class Cita {
    private String id_c, tipo;
    private Date Fecha;
    private Mascotas m;
    private Time duración;
    private Doctor d;
    private String Cédula;
    public Cita(String id, Date F, Mascotas m, Time d, Doctor doc, String ced, String tipo) {
        this.id_c = id;
        this.Fecha = F;
        this.m = m;
        this.duración = d;
        this. d = doc;
        this.Cédula = ced;
        this.tipo = tipo;
    }
    

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cita other = (Cita) obj;
        if (!Objects.equals(this.id_c, other.id_c)) {
            return false;
        }
        return true;
    }

    public String getId_c() {
        return id_c;
    }

    public void setId_c(String id_c) {
        this.id_c = id_c;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date Fecha) {
        this.Fecha = Fecha;
    }

    public Mascotas getM() {
        return m;
    }

    public void setM(Mascotas m) {
        this.m = m;
    }

    public Time getDuración() {
        return duración;
    }

    public void setDuración(Time duración) {
        this.duración = duración;
    }

    public Doctor getD() {
        return d;
    }

    public void setD(Doctor d) {
        this.d = d;
    }

    public String getCédula() {
        return Cédula;
    }

    public void setCédula(String Cédula) {
        this.Cédula = Cédula;
    }
    
}
