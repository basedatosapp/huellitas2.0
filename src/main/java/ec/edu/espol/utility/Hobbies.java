/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.utility;

import ec.edu.espol.common.Mascotas;
import java.util.Objects;


/**
 *
 * @author danny
 */
public class Hobbies {
    private Mascotas m;
    private String hobb;
    public Hobbies(Mascotas m, String hobb) {
        this.m = m;
        this.hobb = hobb;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Hobbies other = (Hobbies) obj;
        if (!Objects.equals(this.m, other.m)) {
            return false;
        }
        return true;
    }

    public Mascotas getM() {
        return m;
    }

    public void setM(Mascotas m) {
        this.m = m;
    }

    public String getHobb() {
        return hobb;
    }

    public void setHobb(String hobb) {
        this.hobb = hobb;
    }
    
}
