/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.db;

import ec.edu.espol.common.Adoptante;
import ec.edu.espol.common.Usuario;
import java.sql.*;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
/**
 *
 * @author danny
 */
public class CreateConnection {
    private final String x = "jdbc:mysql://localhost/huellitasdb?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private final String usuario = "admin";
    private final String clave = "";
    private Connection con;
    private LocalDate ld;
    public CreateConnection() throws SQLException {
        con = DriverManager.getConnection(x, usuario, clave);
    }

    public Connection getCon() {
        return con;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CreateConnection other = (CreateConnection) obj;
        if (!Objects.equals(this.con, other.con)) {
            return false;
        }
        return true;
    }
    
    public boolean validateLogin(String nick, String email, String clave) {
        //String n = "";
        try {
            Statement st = con.createStatement(); System.out.println("Statement");
            ResultSet rs = st.executeQuery(
            /*"select * from usuario where Nick = \"dsoria\" and Correo = \"dsoria@espol.edu.ec\" and Clave = \"dsoria\";"*/
            "select * from usuario where nick = \""+nick+"\" and correo = \""+email+"\" and clave = \""+clave+"\";");       
            if (rs.next()) {
                return nick.equals(rs.getString("Nick"));
            }
            //st.close();
            
        } catch (SQLException ep) {
            System.out.println(ep.toString()); System.out.println("Errorrr");
            System.out.println(ep.getMessage());
        }
        System.out.println("Error");
        return false;
    }
    public Adoptante getAdoptante(Usuario u) {
        String query =
        "select * from usuario as u natural join datos as d natural join adoptante as a\n" +
        "where u.Nick = \""+u.getUsuario()+"\" and a.Nick = \""+u.getUsuario()+"\" and d.Cédula = a.Cédula;";
        System.out.println(query);
        try {
            Statement st = con.createStatement();System.out.println("Statement");
            ResultSet rs = st.executeQuery(query); System.out.println("Ejecutando");
            if(rs.next()) {
                System.out.println("Next");
                System.out.println(rs.getMetaData()); 
                System.out.println(rs.toString()); 
                String[] datos = {rs.getString("Cédula"), rs.getString("NombreCompleto"), rs.getString("Dirección")}; System.out.println("datos");
                System.out.println(Arrays.toString(datos));
                ld = rs.getDate("F_Nac").toLocalDate();
                System.out.println(rs.getDate("F_Nac").toLocalDate().toString());
                Adoptante ad = new Adoptante(datos, rs.getDate("F_Nac").toLocalDate().toString(), u.getDatos());  System.out.println("ad");
                ad.setPath(rs.getString("Foto")); System.out.println("Foto");
                System.out.println(ad.getPath());
                System.out.println(ad.getStrinDatos());
                System.out.println(ad.toString());
                return ad;
            }
            //st.close();
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return null;
    }
    public LocalDate getLD() {
        return ld;
    }
    /*
    public boolean validateLoginNick(String nick, String clave) {
        boolean b = false;
        try {
            Statement st = con.createStatement();
            String query = "select * from usuario where nick = \""+nick+"\" and clave = \""+clave+"\";";
            b = st.execute(query);
            return b;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return b;
        }
    }
    public boolean validateLoginCorreo(String correo, String clave) {
        boolean b = false;
        try {
            Statement st = con.createStatement();
            String query = "select * from usuario where correo = \""+correo+"\" and clave = \""+clave+"\";";
            b = st.execute(query);
            return b;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return b;
        }
    }
    */
    public boolean isNull() {
        return this==null;
    }
}
