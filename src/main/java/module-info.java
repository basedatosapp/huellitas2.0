module ec.edu.espol.huellitas_app {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires java.base;
    requires java.desktop;

    opens ec.edu.espol.huellitas_app to javafx.fxml;
    exports ec.edu.espol.huellitas_app;   
}